import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateAgo'
})
export class DateAgoPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    if (value) {
      const seconds = Math.floor((+new Date() - +new Date(value)) / 1000);
      if (seconds < 29) // less than 30 seconds ago will show as 'Just now'
          return 'Az önce';
      if (seconds > 86400) {
        let date = new Date(value);
        let dateStr = date.toLocaleDateString(['tr-TR']);
        let timeStr = date.toLocaleTimeString(['tr-TR'],{hour: '2-digit',
        minute: '2-digit'})
        let fullDateStr = dateStr + ' ' + timeStr;
        return fullDateStr;
      }
      const intervals = {
          'yıl': 31536000,
          'ay': 2592000,
          'hafta': 604800,
          'gün': 86400,
          'sa': 3600,
          'dk': 60,
          'sn': 1
      };
      let counter;
      for (const i in intervals) {
          counter = Math.floor(seconds / intervals[i]);
          if (counter > 0)
              if (counter === 1) {
                  return counter + ' ' + i + ' önce'; // singular (1 day ago)
              } else {
                  return counter + ' ' + i + ' önce'; // plural (2 days ago)
              }
      }
  }
  return value;
}

}
