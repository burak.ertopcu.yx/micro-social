import { Component, OnInit, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { User } from '../../models/user.model';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'mcs-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit {
  loggedinUser: User
  subscriptions: Subscription[] = [];
  form: FormGroup = new FormGroup({
    message: new FormControl('')
  });

  base64textString:String="";
  mediaUrl:any;

  @Output() newPostAdded = new EventEmitter<any>();
  constructor(private userService:UserService, private httpService: HttpService, private _sanitizer: DomSanitizer, private cdr:ChangeDetectorRef) { }

  ngOnInit(): void {
    this.subscriptions.push(this.userService.loggedinUser.subscribe(user => {
      if (user && user.userId) {
        this.loggedinUser = user;
      } 
    }));
  }

  ngOnDestroy(){
    this.subscriptions.forEach(sb=>sb.unsubscribe());
  }

  sendPost(){
    if (this.form.valid) {
      //go to login api
      const message = this.form.controls['message'].value;
      const userId = this.loggedinUser.userId;
      console.log("::post user:: ", userId, " ::message:: ", message);
      if(!this.base64textString || this.base64textString.trim().length === 0) this.base64textString = null

      const queryParams = { userId: userId, message: message, mediaArr: this.base64textString };
      const apiUrl = 'addNewPost';
      this.subscriptions.push(this.httpService.postWithToken(queryParams, apiUrl).subscribe(data=>{
        console.log("::getAllPosts::", data);
        this.newPostAdded.emit(true);
      }));
    }
  }

  handleFileSelect(evt){
    var files = evt.target.files;
    var file = files[0];
  
  if (files && file) {
      var reader = new FileReader();

      reader.onload =this._handleReaderLoaded.bind(this);

      reader.readAsBinaryString(file);
      
  }
}

_handleReaderLoaded(readerEvt) {
   var binaryString = readerEvt.target.result;
          this.base64textString= btoa(binaryString);
          console.log(btoa(binaryString));
          this.mediaUrl = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
      + this.base64textString);
      console.log("::mediaUrl::",this.mediaUrl);
      this.cdr.detectChanges();
  }

}
