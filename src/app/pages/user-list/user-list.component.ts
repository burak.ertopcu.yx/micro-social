import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { HttpService } from '../../services/http.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  userList:any;

  subscriptions:Subscription[] = [];
  constructor(private httpService: HttpService) { }

  ngOnInit(): void {
    this.getAllUsers();
  }

  ngOnDestroy(){
    this.subscriptions.forEach(sb=>sb.unsubscribe());
  }

  getAllUsers(){
    const apiUrl = 'getAllUsers';  
    const queryParams = {};
    
    this.subscriptions.push(this.httpService.postWithToken(queryParams, apiUrl).subscribe(data=>{
      console.log("::testAllUsers::", data);
      this.userList = data['userList'];
    }));
  }

}
