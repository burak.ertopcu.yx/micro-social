import { Component, Input, OnInit } from '@angular/core';
import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'mcs-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {
  @Input()
  user:User;
  loggedinUser:User;

  subscriptions:any = [];
  constructor(private userService:UserService) { }

  ngOnInit(): void {
    this.getLoggedinUser();
  }

  ngOnDestroy(){
    this.subscriptions.forEach(sb=>sb.unsubscribe());
  }

  getLoggedinUser(){
    this.subscriptions.push(this.userService.loggedinUser.subscribe(user=>{
      if(user && user.userId){
        this.loggedinUser = user;
      }
    }));
  }

}
