import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { HttpService } from '../../services/http.service';
import { faWineGlass } from '@fortawesome/free-solid-svg-icons';
import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  subscriptions: Subscription[] = [];
  postList: any;
  isAddNewPost:boolean;
  currentUser:User;
  isLoading:boolean = false;

  constructor(private http: HttpClient, private httpService: HttpService, private userService: UserService, private cdr:ChangeDetectorRef) { }

  ngOnInit(): void {
    console.log("::home comp::");
    this.subscriptions.push(this.userService.loggedinUser.subscribe(user=>{
      if(user && user.userId){
        this.currentUser = user;
        this.getAllPosts();
      }
    }));
  }

  ngOnDestroy(){
    this.subscriptions.forEach(sb=>sb.unsubscribe());
  }

  getAllPosts(){
    console.log("::getAllPosts running!::");
    this.isLoading = true;
    //const host = window.location.hostname + ':5000';
    const apiUrl = 'getAllPosts';  
    const queryParams = {userId: this.currentUser.userId};
    
    this.subscriptions.push(this.httpService.postWithToken(queryParams, apiUrl).subscribe(data=>{
      console.log("::getAllPosts::", data);
      this.postList = data['postList'];
      this.isLoading = false;
      this.cdr.detectChanges();
    }, error=> {
      console.log("::getAllPosts post sub. error::", error.message);
      this.isLoading = false;
    }));
  }

  testAllUsers() {
    const apiUrl = 'getAllUsers';  
    const queryParams = {};
    
    this.subscriptions.push(this.httpService.postWithToken(queryParams, apiUrl).subscribe(data=>{
      console.log("::testAllUsers::", data);
    }));
    
    /* this.subscriptions.push(this.http.post<any>('http://localhost:5000/getAllUsers', queryParams, queryHeaders).subscribe(data => {
      console.log("::testAllUsers::", data);
    })); */
  }

  setIsAddNewPost(){
    if(!this.isAddNewPost){
      this.isAddNewPost = true;
    }else{
      this.isAddNewPost = false;
    }
  }

  newPostAdded(data){
    if(data){
      this.isAddNewPost = false;
      this.getAllPosts();
    }
  }

  postMessageEditted(data){
    if(data){
      this.getAllPosts();
    }
  }

  postLiked(data){
    if(data){
      this.getAllPosts();
    }
  }

  postUnliked(data){
    if(data){
      this.getAllPosts();
    }
  }

}
