import { Component, OnInit, Input, ChangeDetectorRef, Output, EventEmitter } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { HttpService } from '../../services/http.service';
import { User } from '../../models/user.model';
import { Subscription } from 'rxjs';
import Swal from 'sweetalert2';
import { UserService } from '../../services/user.service';


@Component({
  selector: 'mcs-post-card',
  templateUrl: './post-card.component.html',
  styleUrls: ['./post-card.component.scss']
})
export class PostCardComponent implements OnInit {
  @Input()
  postData:any;

  currentUser:User;
  
  mediaUrl:any;

  isEditModeOn:boolean = false;
  isSaveEditLoading:boolean = false;
  messageToEdit:string;

  subscriptions:Subscription[] = [];

  constructor(private _sanitizer: DomSanitizer, private cdr:ChangeDetectorRef, private httpService:HttpService, private userService:UserService) { }

  ngOnInit(): void {
    if(!this.currentUser){
      this.subscriptions.push(this.userService.loggedinUser.subscribe(user=>{
        if(user && user.userId){
          this.currentUser = user;
        }
      }));
    }
    
    if(this.postData && this.postData.mediaArray && this.postData.mediaArray.length > 0){
      this.mediaUrl = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' 
      + this.postData.mediaArray);
      console.log("::mediaUrl::",this.mediaUrl);
      this.cdr.detectChanges();
    }
  }

  openEditMode(message:string){
    if(this.isEditModeOn){
      this.closeEditMode();
      return;
    }
    this.messageToEdit = message;
    this.isEditModeOn = true;
    this.cdr.detectChanges();
  }

  closeEditMode(){
    this.isEditModeOn = false;
  }

  saveEdit(){
    if(!this.messageToEdit || this.messageToEdit.trim().length === 0){
      return;
    }
    this.isSaveEditLoading = true;
    this.closeEditMode();
    const apiUrl = 'editPost';  
    const queryParams = {postId:this.postData.postId, userId:this.postData.userId, message:this.messageToEdit};
    
    this.subscriptions.push(this.httpService.postWithToken(queryParams, apiUrl).subscribe(data=>{
      console.log("::editPost::", data);
      this.postData.message = this.messageToEdit;
      //this.postMessageUpdate.emit(1);
      this.isSaveEditLoading = false;
      //Swal.fire('success', 'Başarıyla kaydedildi!', 'success');
    }));
  }

  ngOnDestroy(){
    this.subscriptions.forEach(sb=>sb.unsubscribe());
  }

  messageEditted(){
    console.log("::mess editted!::", this.messageToEdit);
  }

  likePost(){
    //const host = window.location.hostname + ':5000';
    const apiUrl = 'likePost';  
    const queryParams = {postId:this.postData.postId, userId:this.currentUser.userId};
    
    this.subscriptions.push(this.httpService.postWithToken(queryParams, apiUrl).subscribe(data=>{
      console.log("::likePost::", data);
      this.postData.isLiked = true;
      this.postData.likeCount = this.postData.likeCount + 1;
    }, error=> {
      console.log("::cannot like post!::", error);
    }));
  }

  unlikePost(){
    //const host = window.location.hostname + ':5000';
    const apiUrl = 'unlikePost';  
    const queryParams = {postId:this.postData.postId, userId:this.currentUser.userId};
    
    this.subscriptions.push(this.httpService.postWithToken(queryParams, apiUrl).subscribe(data=>{
      console.log("::unlikePost::", data);
      this.postData.isLiked = false;
      this.postData.likeCount = this.postData.likeCount - 1;
    }, error=> {
      console.log("::cannot unlike post!::", error);
    }));
  }

}
