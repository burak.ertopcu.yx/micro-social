import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserService } from 'src/app/services/user.service';
import { FormGroup, FormControl } from '@angular/forms';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  subscriptions: Subscription[] = [];
  isShowLogin: boolean = false;
  form: FormGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl(''),
  });

  constructor(private router: Router, private http: HttpClient, private userService: UserService, private httpService: HttpService) { }

  ngOnInit(): void {
    console.log("::login comp::");
    this.checkLoggedinUser();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sb => sb.unsubscribe());
  }

  checkLoggedinUser() {
    //const sid = sessionStorage.getItem('sid');
    this.subscriptions.push(this.userService.loggedinUser.subscribe(user => {
      if (user && user.userId) {
        this.router.navigateByUrl('home');
      } else {
        this.isShowLogin = true;
        //this.testAllUsers();
      }
    }));
  }

  testAllUsers() {
    var headerOpts = {'Content-Type': 'application/json'};
    if(sessionStorage.getItem('sid')) headerOpts['Authorization'] = 'Bearer ' + sessionStorage.getItem('sid');
    var reqHeader = new HttpHeaders(headerOpts);
    const queryParams = {}
    const queryHeaders = { headers: reqHeader };
    console.log("::testAllUsers queryHeaders::",queryHeaders);
    this.subscriptions.push(this.http.post<any>('http://localhost:5000/getAllUsers', queryParams, queryHeaders).subscribe(data => {
      console.log("::testAllUsers::", data);
    }));
  }

  submit() {
    if (this.form.valid) {
      //go to login api
      const credText:string = this.form.controls['email'].value;
      const pwd:string = this.form.controls['password'].value;
      //console.log("::email:: ", email, " ::pwd:: ", pwd);
      const isEmail:Boolean = credText && credText.includes("@") ? true : false; 

      const queryParams = { email: isEmail ? credText : null, uname: !isEmail ? credText : null, pwd: pwd };
      //const apiUrl = 'http://localhost:5000/login';
      //const host = window.location.hostname + ':5000';
      const apiUrl = 'login'; 
      this.subscriptions.push(this.httpService.postWithoutToken(queryParams, apiUrl).subscribe(data=>{
        if (data && data['user'] && data['sid']) {
          this.userService.dispatch(data['user']);
          sessionStorage.setItem('sid', data['sid']);
          this.httpService.dispatchSid(data['sid']);
          console.log("::sid ok::");
        } else {
          console.log("::error::",data);
        }
      }));
      /* this.subscriptions.push(this.http.post<any>('http://localhost:5000/login', queryParams).subscribe(data => {
        if (data && data.user && data.sid) {
          this.userService.dispatch(data.user);
          sessionStorage.setItem('sid', data.sid);
          this.httpService.dispatchSid(data.sid);
        } else {
          Swal.fire({
            title: 'Error!',
            text: data,
            confirmButtonText: 'OK'
          });
        }
      })); */
    }
  }

}
