import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root',
})

export class HttpService {
    api = 'http://185.148.240.82:5000/';
    sid = sessionStorage.getItem('sid');
    httpOptionsWithToken = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.sid
    };

    httpOptionsWithoutToken = {
        'Content-Type': 'application/json',
    };

    queryHeadersWithToken = { headers: this.httpOptionsWithToken };
    queryHeadersWithoutToken = { headers: this.httpOptionsWithoutToken };

    constructor(private http:HttpClient){}

    dispatchSid(sid:string){
        this.sid = sid;
    }

    login(queryParams, apiUrl?) {
        let body = JSON.stringify(queryParams);
        let api = apiUrl ? apiUrl : this.api;
        return this.http.post(api+'/login', body, this.queryHeadersWithoutToken);
    }

    postWithToken(queryParams, apiUrl:string) {
        return this.postWithoutToken(queryParams, apiUrl);
        /* if(apiUrl){
            let url = this.api + apiUrl;
            this.queryHeadersWithToken.headers.Authorization = 'Bearer '+sessionStorage.getItem('sid');
            let body = JSON.stringify(queryParams);
            console.log("::queryHeadersWithToken::", this.queryHeadersWithToken.headers);
            return this.http.post(url, body, this.queryHeadersWithToken);
        } */
    }

    postWithoutToken(queryParams, apiUrl) {
        if(apiUrl){
            let url = this.api + apiUrl;
            //if(apiUrl.includes('localhost')) apiUrl.replace('localhost', '192.168.1.38')
            let body = JSON.stringify(queryParams);
            //console.log("::url::", url);
            return this.http.post(url, body, this.queryHeadersWithoutToken);
        }
    }


}