import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/user.model';

@Injectable({
    providedIn: 'root',
})

export class UserService{
    loggedinUser: BehaviorSubject<User> = new BehaviorSubject(new User());

    dispatch(user:User){
        this.loggedinUser.next(user);
    }

    currentUser(){
        return this.loggedinUser.value;
    }
}