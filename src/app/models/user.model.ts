
export class User{
    userId:number;
    email:string;
    username:string;
    fullName:string;
    type:number;
    createdTime:number;
    modifiedTime:number;
}