import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { faHome, faUsers, faBook, faHeart } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { Subscription } from 'rxjs';
import { User } from '../models/user.model';
import { HttpClient } from '@angular/common/http';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'mcs-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  showMenu = true;
  homeIcon = faHome;
  usersIcon = faUsers;
  postsIcon = faBook;
  favirotesIcon = faHeart;
  activeItem = 'home';
  loggedinUserFName;
  subscriptions:Subscription[] = []
  loggedinUser: User;
  constructor(private router: Router, private cdr: ChangeDetectorRef, private userService: UserService, private http:HttpClient, private httpService:HttpService) { }

  ngOnInit(): void {
    this.toggleMenu();
    const sid = sessionStorage.getItem('sid') ? sessionStorage.getItem('sid') : undefined;
    this.subscriptions.push(this.userService.loggedinUser.subscribe(user=>{
      if(user && user.userId){
        this.loggedinUser = user;
        this.loggedinUserFName = this.loggedinUser.fullName;
      }else{
        if(sid) {
          console.log("::sid exists::");
          this.getLoggedinUser(sid);
        }else{
          this.router.navigateByUrl('login');
        }
      }
    }));
  }

  ngOnDestroy(){
    this.subscriptions.forEach(sb=>sb.unsubscribe());
  }

  checkLogin(){
    console.log("::navbar comp:: checkLogin");
    const sid = sessionStorage.getItem('sid');
    if(!sid) {
      console.log("::navbar comp:: !sid");
      this.router.navigateByUrl('/login');
    } else {
      this.getLoggedinUser(sid);
    }
    this.cdr.detectChanges();
  }

  getLoggedinUser(sid){
    console.log("::sid::",sid);
    const queryParams = { sid: sid};
    //const host = window.location.hostname + ':5000';
    const apiUrl = 'getLoggedinUser'; 
    this.subscriptions.push(this.httpService.postWithoutToken(queryParams,apiUrl).subscribe(data => {
        if(data && data['user']){
          this.loggedinUser = data['user'];
          this.loggedinUserFName = this.loggedinUser.fullName;
          this.userService.dispatch(this.loggedinUser);
        }
    }));
  }

  toggleMenu(){
    if(this.showMenu){
      this.showMenu = false;
    }else{
      this.showMenu = true;
    }
  }

  menuItemClicked(item){
    this.activeItem = item;
    console.log("::activeItem::", this.activeItem);
    if(this.activeItem){
      if(this.activeItem === "users"){
        this.router.navigateByUrl('user-list');
      }else if(this.activeItem === "home"){
        this.router.navigateByUrl('home');
      }
    }
  }

  logout(){
    const sid = sessionStorage.getItem('sid');
    console.log("::burda::");
    if(this.loggedinUser && this.loggedinUser.email && sid){
      const queryParams = {email: this.loggedinUser.email, sid: sid};
      //const host = window.location.hostname + ':5000';
      const apiUrl = 'logout'; 
      this.subscriptions.push(this.httpService.postWithToken(queryParams, apiUrl).subscribe(data => {
        if(data && data['success']){
          this.loggedinUser = null;
          this.loggedinUserFName = undefined;
          sessionStorage.removeItem('sid');
          this.userService.dispatch(this.loggedinUser);
          console.log("::loggedinUser from service after logout::", this.userService.currentUser());
          this.router.navigateByUrl('/login');
        }
    }));
    }
  }

}
